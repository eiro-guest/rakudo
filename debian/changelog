rakudo (2021.09-1) unstable; urgency=medium

  * New upstream version 2021.09
  * Remove Robert from Uploaders (Closes: 994653)
  * control: use debhelper 13
  * control: declare compliance with policy 4.6.0
  * control: update moarvm-dev and nqp dep versions (cme)

 -- Dominique Dumont <dod@debian.org>  Sun, 26 Sep 2021 16:38:42 +0200

rakudo (2020.12+dfsg-1) unstable; urgency=medium

  * New upstream version 2020.12+dfsg
  * control: update moarvm-dev and nqp dep versions (cme)
  * remove fix-pod patch (applied upstream)

 -- Dominique Dumont <dod@debian.org>  Fri, 08 Jan 2021 15:43:41 +0100

rakudo (2020.11-1) unstable; urgency=medium

  * New upstream version 2020.11
  * control: update moarvm-dev and nqp dep versions (cme)
  * declare compliance with policy 4.5.1
  * update fix-pod patch

 -- Dominique Dumont <dod@debian.org>  Wed, 02 Dec 2020 11:53:29 +0100

rakudo (2020.10-1) unstable; urgency=medium

  * New upstream version 2020.10
  * add patch to fix pod error
  * control: update moarvm-dev and nqp dep versions (cme)

 -- Dominique Dumont <dod@debian.org>  Sun, 01 Nov 2020 15:47:41 +0100

rakudo (2020.09+dfsg-2) unstable; urgency=medium

  * requires nqp (>= 2020.09+dfsg-4) (Closes: 971769)

 -- Dominique Dumont <dod@debian.org>  Fri, 09 Oct 2020 18:34:07 +0200

rakudo (2020.09+dfsg-1) unstable; urgency=medium

  * New upstream version 2020.09+dfsg (Closes: 969578)
  * exclude t/spec directory
  * refreshed copyright with cme
  * control: update moarvm-dev and nqp dep versions (cme)
  * watch: add dversionmangle=auto option

 -- Dominique Dumont <dod@debian.org>  Sun, 27 Sep 2020 17:38:04 +0200

rakudo (2020.08.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Dominique Dumont ]
  * New upstream version 2020.08.2
  * control: update moarvm-dev and nqp dep versions (cme)
  * override lintian error related to rpath
  * add missing symlink to man pages
  * add patch to hardcode raku path in script

 -- Dominique Dumont <dod@debian.org>  Thu, 03 Sep 2020 18:37:57 +0200

rakudo (2020.06-1) unstable; urgency=medium

  * New upstream version 2020.06
  * control: update moarvm-dev and nqp dep versions (cme)

 -- Dominique Dumont <dod@debian.org>  Sat, 27 Jun 2020 15:24:17 +0200

rakudo (2020.05.1-1) unstable; urgency=medium

  * New upstream version 2020.05.1
  * rm fix-raku-build.patch (applied upstream)
  * update copyright with cme
  * control: update moarvm-dev and nqp dep versions (cme)
  * rakudo-helper: cleanup empty /usr/lib/perl6/vendor/resources/
    when removing modules (Closes: #915785)

 -- Dominique Dumont <dod@debian.org>  Sun, 10 May 2020 15:21:42 +0200

rakudo (2020.02.1-2) unstable; urgency=medium

  * control: add missing libipc-system-simple-perl dependency
    (Closes: 959000)
  * cleanup old vendor dir in postinst instead of postrm

 -- Dominique Dumont <dod@debian.org>  Mon, 04 May 2020 18:42:44 +0200

rakudo (2020.02.1-1) unstable; urgency=medium

  * New upstream version 2020.02.1
  * control: update moarvm-dev and nqp dep versions (cme)
  * control: declare compliance with policy 4.5.0
  * use new style of debhelper dependency
  * remove convenience copy of nqp-configure
  * refreshed copyright file with cme
  * rules:
    * invoke raku instead of perl6
    * install perl6 files in /usr/lib/perl6
  * lintian: allow rpath in perl6 rakudo exe
  * watch: use release'd file
  * watch: verify tarball signature after download
  * replace samcv key with Alexander Kiryuhin's
  * add patch from upstream to fix installation of rakudo in /usr/lib/perl6
  * rules:
    * use rakudo-m to get compiler-id
    * disable perf tests as suggested upstream
  * rakudo-helper.pl:
    * vendor dir is in /usr/lib
    * installed without template
    * use autodie to trap system() errors
    * use more widely Path::Tiny
  * postrm: cleanup old vendor dir

 -- Dominique Dumont <dod@debian.org>  Sun, 26 Apr 2020 17:50:54 +0200

rakudo (2019.11-4) unstable; urgency=medium

  * patch perf tests for mipsel

 -- Dominique Dumont <dod@debian.org>  Wed, 08 Jan 2020 10:52:47 +0100

rakudo (2019.11-3) unstable; urgency=medium

  * rules: setup verbose tests

 -- Dominique Dumont <dod@debian.org>  Tue, 07 Jan 2020 14:34:41 +0100

rakudo (2019.11-2) unstable; urgency=medium

  * Update my mail address.
  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Sat, 28 Dec 2019 15:21:19 +0800

rakudo (2019.11-1) experimental; urgency=medium

  * New upstream version 2019.11
  * Bump embedded nqp-configure to 1cd8bdc7.
  * Bump moarvm & nqp verison requirement to (>= 2019.11~).
  * override dh_missing to list missing files.

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 26 Dec 2019 14:39:49 +0800

rakudo (2019.07.1-8) unstable; urgency=medium

  * postinst: reinstall all modules when upgrading from << 2019.07.1-6
  * maintscripts: Properly quote the shell variables.
  * Apply wrap-and-sort.
  * Upload to unstable (Closes: #942364)

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 17 Oct 2019 15:39:50 +0000

rakudo (2019.07.1-7) experimental; urgency=medium

  * Rakudo-helper: -R invalid due to empty graph when no module has dependency.
  * Rakudo-helper: Make reinstallation of modules verbose.
  * Rakudo-helper: report progress while reinstalling all modules.
  * preinst: Purge /usr/lib/perl6/vendor gracefully.

 -- Mo Zhou <cdluminate@gmail.com>  Wed, 16 Oct 2019 13:08:29 +0800

rakudo (2019.07.1-6) experimental; urgency=medium

  * Deal with perl6-* packages for proper /usr/{lib->share} transition.
  * postrm: fix upgrade due to missing "shell case" conditions.
  * Add dockerfile rakudo.docker for testing install/upgrade.

 -- Mo Zhou <cdluminate@gmail.com>  Tue, 15 Oct 2019 09:05:43 +0000

rakudo (2019.07.1-5) experimental; urgency=medium

  * Move runtimes/vendor to /usr/share in sync with rakudostar.
  * postrm: remove residual empty directories.

 -- Mo Zhou <cdluminate@gmail.com>  Tue, 15 Oct 2019 14:18:53 +0800

rakudo (2019.07.1-4) unstable; urgency=medium

  * Revert "Move files from /usr/share/perl6 to /usr/lib/perl6 and
    create symlink.", changing /usr/share/perl6 back into a directory.
    Also remove the dir->symlink conversion. (Closes: #940776, #940774)
  * Workaround the /usr/share/.*.moarvm file not found error.

 -- Mo Zhou <cdluminate@gmail.com>  Sat, 28 Sep 2019 20:59:22 +0800

rakudo (2019.07.1-3) unstable; urgency=medium

  * Bump Standards-Version to 4.4.0 (no change).
  * Upload to unstable. (Closes: #935290)

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 19 Sep 2019 04:05:03 +0000

rakudo (2019.07.1-2) experimental; urgency=medium

  * Add dpkg helper to change /usr/share/perl6 into a symlink.

 -- Mo Zhou <cdluminate@gmail.com>  Wed, 18 Sep 2019 22:49:53 +0800

rakudo (2019.07.1-1) experimental; urgency=medium

  [ Mo Zhou ]
  * New upstream version 2019.07.1
  * Import convenient copy of nqp-configure (7411804).
  * Link debian/nqp-configure to 3rdparty before configure.
  * Bump moarvm-dev and nqp requirment to (>= 2019.07)
  * Move files from /usr/share/perl6 to /usr/lib/perl6 and create symlink.
  * Guard dh_auto_test with "nocheck" flag detector.
  * Fixup build failure with sbuild.

  [ Dominique Dumont ]
  * control: test rules-requires-root

 -- Mo Zhou <cdluminate@gmail.com>  Fri, 13 Sep 2019 16:11:49 +0000

rakudo (2019.03.1-1) experimental; urgency=medium

  * New upstream version 2019.03.1
  * Point watch to Github.
  * Add myself to Uploaders.
  * Bump moarvm and nqp requirement to (>= 2019.03).

 -- Mo Zhou <cdluminate@gmail.com>  Fri, 12 Jul 2019 12:06:57 +0000

rakudo (2018.12-5) unstable; urgency=medium

  * Execute last test run without spesh

 -- Robert Lemmen <robertle@semistable.com>  Wed, 09 Jan 2019 18:59:57 +0100

rakudo (2018.12-4) unstable; urgency=medium

  * Run tests more often on failures to get data on flappers

 -- Robert Lemmen <robertle@semistable.com>  Fri, 04 Jan 2019 20:02:29 +0100

rakudo (2018.12-3) unstable; urgency=medium

  * Remove unused libreadline dependency

 -- Robert Lemmen <robertle@semistable.com>  Mon, 31 Dec 2018 10:52:35 +0100

rakudo (2018.12-2) unstable; urgency=medium

  * No longer fail if precomp files do not exist on removal (closes: #916646)
  * Make vendor/resources explicit in package (closes: #915785)

 -- Robert Lemmen <robertle@semistable.com>  Sun, 30 Dec 2018 12:54:52 +0100

rakudo (2018.12-1) unstable; urgency=medium

  * New upstream version 2018.12
  * control: update moarvm-dev and nqp dep versions

 -- Robert Lemmen <robertle@semistable.com>  Sat, 22 Dec 2018 14:51:21 +0100

rakudo (2018.11-1) unstable; urgency=medium

  * New upstream version 2018.11
  * control: update moarvm-dev and nqp dep versions

 -- Robert Lemmen <robertle@semistable.com>  Sat, 01 Dec 2018 19:09:33 +0100

rakudo (2018.10-1) unstable; urgency=medium

  * New upstream version 2018.10
  * control: update moarvm-dev and nqp dep versions

 -- Robert Lemmen <robertle@semistable.com>  Thu, 15 Nov 2018 21:15:18 +0100

rakudo (2018.09-2) unstable; urgency=medium

  * Fix install-dist.p6 location in rakudo-helper.pl (closes: #909698)

 -- Robert Lemmen <robertle@semistable.com>  Fri, 28 Sep 2018 10:17:52 +0200

rakudo (2018.09-1) unstable; urgency=medium

  * New upstream version 2018.09
  * control: update moarvm-dev and nqp dep versions

 -- Robert Lemmen <robertle@semistable.com>  Fri, 28 Sep 2018 09:29:41 +0200

rakudo (2018.06-1) unstable; urgency=medium

  * New upstream version 2018.06
  * control: update moarvm-dev and nqp dep versions
  * postinst script and rakudo-helper to support upgrades
    and module package precompilation and cleanup

 -- Robert Lemmen <robertle@semistable.com>  Wed, 27 Jun 2018 19:03:19 +0200

rakudo (2018.05-1) unstable; urgency=medium

  * New upstream version 2018.05
  * control: update moarvm-dev and nqp dep versions
  * add rakudo-helper script

 -- Robert Lemmen <robertle@semistable.com>  Sat, 26 May 2018 14:14:12 +0200

rakudo (2018.04.1-1) unstable; urgency=medium

  * New upstream version 2018.04.1
  * control: update moarvm-dev and nqp dep versions

 -- Robert Lemmen <robertle@semistable.com>  Mon, 30 Apr 2018 09:23:09 +0200

rakudo (2018.04-1) unstable; urgency=medium

  [ Dominique Dumont ]
  * rules: relax dep on nqp (Closes: #895180)

  [ Robert Lemmen ]
  * New upstream version 2018.04
  * Bump Standards-Version
  * control: update moarvm-dev and nqp dep versions

 -- Robert Lemmen <robertle@semistable.com>  Thu, 26 Apr 2018 09:03:34 +0200

rakudo (2018.03-2) unstable; urgency=medium

  * Rebuild to depend on current nqp

 -- Robert Lemmen <robertle@semistable.com>  Tue, 10 Apr 2018 15:04:11 +0200

rakudo (2018.03-1) unstable; urgency=medium

  * New upstream version 2018.03
  * control: update moarvm-dev and nqp dep versions (cme)
  * control: update Vcs-Browser and Vcs-Git
  * update copyright info (cme)
  * add instructions about signature file in README.source
  * refreshed patch

 -- Dominique Dumont <dod@debian.org>  Tue, 03 Apr 2018 19:33:16 +0200

rakudo (2018.02.1-3) unstable; urgency=medium

  * rules: run tests one at a time
  * patch repl test (Closes: #880795)

 -- Dominique Dumont <dod@debian.org>  Tue, 20 Mar 2018 18:24:29 +0100

rakudo (2018.02.1-2) unstable; urgency=medium

  * control: breaks and replaces rakudo-lib (Closes: #891693)

 -- Dominique Dumont <dod@debian.org>  Wed, 28 Feb 2018 17:17:12 +0100

rakudo (2018.02.1-1) unstable; urgency=medium

  * setup gpg check of upstream tarball
  * New upstream version 2018.02.1
  * control: update moarvm-dev and nqp dep versions (cme)

 -- Dominique Dumont <dod@debian.org>  Sun, 25 Feb 2018 14:32:50 +0100

rakudo (2018.01-1) unstable; urgency=medium

  * New upstream version 2018.01
  * control: update moarvm-dev and nqp dep versions (cme)
  * control: declare compliance with policy 4.1.3
  * refreshed copyright info with cme
  * rules: fix libdir

 -- Dominique Dumont <dod@debian.org>  Wed, 07 Feb 2018 19:07:06 +0100

rakudo (2017.10-1) unstable; urgency=medium

  * New upstream version 2017.10
  * control: remove rakudo-lib package
  * control: update moarvm-dev and nqp dep versions (cme)
  * control: declare compliance with policy 4.1.1
  * disable obsolete fix-lib-install-dir patch
  * rules: use --libdir=/usr/lib/perl6 option. Many thanks to
    Gerd Pokorra from Fedora for the help.
  * rules: change install dir
  * remove debian/install
  * add link for perl6-lldb-m man page

 -- Dominique Dumont <dod@debian.org>  Wed, 01 Nov 2017 19:29:28 +0100

rakudo (2017.06-1) unstable; urgency=medium

  [ Daniel Dehennin ]
  * New upstream version 2017.06
  * control: update moarvm-dev and nqp dep versions

  [ Dominique Dumont ]
  * control: Declare compliance with policy 4.0.0

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Sat, 24 Jun 2017 00:29:57 +0200

rakudo (2017.05-1) experimental; urgency=medium

  [ Daniel Dehennin ]
  * New upstream version 2017.05
  * control: update moarvm-dev and nqp dep versions
  * Add myself to Uploaders

 -- Dominique Dumont <dod@debian.org>  Wed, 24 May 2017 16:31:10 +0200

rakudo (2017.03-2) experimental; urgency=medium

  * control: update moarvm-dev and nqp dep versions (cme)

 -- Dominique Dumont <dod@debian.org>  Thu, 06 Apr 2017 19:20:09 +0200

rakudo (2017.03-1) experimental; urgency=medium

  * New upstream version 2017.03

 -- Dominique Dumont <dod@debian.org>  Fri, 31 Mar 2017 14:00:27 +0200

rakudo (2017.02-2) experimental; urgency=medium

  [ Dominique Dumont ]
  * don't install core lib in /usr/share/perl6/lib

 -- Dominique Dumont <dod@debian.org>  Sat, 18 Mar 2017 15:21:39 +0100

rakudo (2017.02-1) experimental; urgency=medium

  * New upstream version 2017.02
  * control: update moarvm-dev and nqp dep versions (cme)
  * refreshed patches
  * copyright: updated with cme update
  * fix install lib dir in patch
  * remove unused lintian-overrides entries
  * rules: remove empty directories after build

 -- Dominique Dumont <dod@debian.org>  Sun, 05 Mar 2017 18:52:46 +0100

rakudo (2016.12-1) unstable; urgency=medium

  * New upstream version 2016.12
  * refreshed patches
  * control: update moarvm-dev and nqp dep versions (cme)

 -- Dominique Dumont <dod@debian.org>  Tue, 20 Dec 2016 13:33:04 +0100

rakudo (2016.11-1) unstable; urgency=medium

  * New upstream version 2016.11
  * removed patch (applied upstream)
  * refreshed patch
  * control: update moarvm-dev and nqp dep versions (cme)

 -- Dominique Dumont <dod@debian.org>  Mon, 28 Nov 2016 13:48:20 +0100

rakudo (2016.10-1) unstable; urgency=medium

  Many thanks to Tobias Leich (FROGGS) who did a terrific job
  porting rakudo on all architectures supported by Debian.
  * New upstream version 2016.10
  * control:
    * updated moarvm-dev and nqp dep versions (cme)
    * enable mips mipsel mips64el (Closes: #841346)
    * enable s390x
  * update patch to tweak pod doc

 -- Dominique Dumont <dod@debian.org>  Mon, 31 Oct 2016 09:27:45 +0100

rakudo (2016.09-2) unstable; urgency=medium

  * control: remove provides perl6
  * patch to fix man page generation (Closes: #839059)
  * control: BD on moarvm-dev 2016.09+dfsg-2
    (to fix arm and x32 builds)

 -- Dominique Dumont <dod@debian.org>  Mon, 10 Oct 2016 10:14:30 +0200

rakudo (2016.09-1) unstable; urgency=medium

  [ Daniel Dehennin ]
  * New upstream version 2016.09
  * control: updated moarvm-dev and nqp dep versions
  * Update patch

  [ Dominique Dumont ]
  * copyright: refreshed with cme

 -- Dominique Dumont <dod@debian.org>  Tue, 27 Sep 2016 13:12:09 +0200

rakudo (2016.07.1-1) unstable; urgency=medium

  * Imported Upstream version 2016.07.1
  * Refresh patch
  * control: updated moarvm-dev and nqp dep versions
  * copyright: updated with cme

 -- Daniel Dehennin <daniel.dehennin@baby-gnu.org>  Sat, 23 Jul 2016 15:45:55 +0200

rakudo (2016.06-1) unstable; urgency=medium

  * Imported Upstream version 2016.06
  * use cme function in update-control.pl
  * control: updated moarvm-dev and nqp dep versions (cme)
  * use simple BTS url in patch header (cme)
  * copyright: use https for Format parameter (cme)

 -- Dominique Dumont <dod@debian.org>  Thu, 07 Jul 2016 13:11:00 +0200

rakudo (2016.05-2) unstable; urgency=medium

  * added lintian overrides for /usr/share/perl6/site/lib

 -- Dominique Dumont <dod@debian.org>  Thu, 02 Jun 2016 10:51:24 +0200

rakudo (2016.05-1) unstable; urgency=medium

  * Imported Upstream version 2016.05
  * control: updated moarvm-dev and nqp dep versions
  * merge fix-lib-dir-in-configure patch into fix-lib-install-dir
  * fix inconsistencies in fix-lib-install-dir patch

 -- Dominique Dumont <dod@debian.org>  Mon, 30 May 2016 13:53:12 +0200

rakudo (2016.04-1) unstable; urgency=medium

  * Imported Upstream version 2016.04
  * added debian/update-control.pl script...
  * control:
    * updated moarvm-dev and nqp dep versions
    * Standards-Version: '3.9.7' -> '3.9.8'

 -- Dominique Dumont <dod@debian.org>  Mon, 25 Apr 2016 13:56:27 +0200

rakudo (2016.03-1) unstable; urgency=medium

  * Imported Upstream version 2016.03
  * copyright: refreshed with cme update
  * control: build dep on moarvm-dev (>=2016-03)

 -- Dominique Dumont <dod@debian.org>  Sat, 02 Apr 2016 11:56:08 +0200

rakudo (2016.02-1) unstable; urgency=medium

  * Imported Upstream version 2016.02
  * refreshed patches
  * removed glossary.pod from docs (removed upstream)
  * control:
    * updated build dep on moarvm-dev (>=2016-02)
    * updated Standard-Version
    *  updated Vcs-Git to use https

 -- Dominique Dumont <dod@debian.org>  Tue, 01 Mar 2016 21:14:57 +0100

rakudo (2015.11-2) unstable; urgency=medium

  * patch to fix lib path (Closes: #809372)
  * control: removed mipsel arch
  * control: updated version of moarvm-dev and nqp build deps
    Dependencies are restricted to current upstream version
    (i.e. 2015.11* for this version)
  * rules: restrict run-time dep on moarvm

 -- Dominique Dumont <dod@debian.org>  Thu, 07 Jan 2016 18:38:48 +0100

rakudo (2015.11-1) unstable; urgency=medium

  * control: add dep on libkvm-dev and libffi-dev (Closes: #801478)
  * Imported Upstream version 2015.11
  * refreshed fix-lib-install-dir patch

 -- Dominique Dumont <dod@debian.org>  Sun, 27 Dec 2015 18:34:32 +0100

rakudo (2015.09-2) unstable; urgency=medium

  * rules: fix nqp dependency setup
  * README.source: added explanations on nqp dependency

 -- Dominique Dumont <dod@debian.org>  Sat, 10 Oct 2015 10:27:39 +0200

rakudo (2015.09-1) unstable; urgency=medium

  * Imported Upstream version 2015.09
  * split rakudo package in 2: rakudo and rakudo-lib.
  * Use moarvm backend instead of parrot
  * control:
    + added dep on libatomic-ops-dev libtommath-dev libuv1-dev
    * depends on nqp >= 2015.09.1-2
    + depends on moarvm >= 2015.09-3
    - removed dependency on parrot
    - dropped libicu-dev dependency.
    * suggest valgrind for perl6-valgrind-m
    * support the same arch as moarvm
    * updated std-version
  * copyright: updated years
  * added patch to deliver .so files in /usr/lib/perl6
    Thanks FROGGS for the help
  * rules: use moar backend instead of parrot

 -- Dominique Dumont <dod@debian.org>  Wed, 07 Oct 2015 20:06:42 +0200

rakudo (2014.07-4) unstable; urgency=medium

  * control: reduced supported arch to the ones working with nqp

 -- Dominique Dumont <dod@debian.org>  Tue, 16 Sep 2014 10:32:55 +0200

rakudo (2014.07-3) unstable; urgency=medium

  * Put back dependency on exact version of nqp
  * added a README.source to explain why

 -- Dominique Dumont <dod@debian.org>  Sun, 14 Sep 2014 13:07:46 +0200

rakudo (2014.07-2) unstable; urgency=medium

  * rules: now use a versioned >= dep on nqp

 -- Dominique Dumont <dod@debian.org>  Fri, 29 Aug 2014 15:14:44 +0200

rakudo (2014.07-1) unstable; urgency=medium

  * Imported Upstream version 2014.07
  * control:
    * fixed Vcs-Browser url to use cgit
    * updated version of nqp and parrot dependencies

 -- Dominique Dumont <dod@debian.org>  Thu, 28 Aug 2014 12:54:21 +0200

rakudo (2014.03.01-1) unstable; urgency=low

  [ Alessandro Ghedini ]
  * Remove myself from Uploaders

  [ Dominique Dumont ]
  * Imported Upstream version 2014.03.01
  * copyright: updated years
  * added links for perl-p and perl-debug-p man page
  * control: updated BD on nqp and parrot, added libgmp-dev

 -- Dominique Dumont <dod@debian.org>  Sat, 12 Apr 2014 12:26:28 +0200

rakudo (2014.01-1) unstable; urgency=medium

  * New upstream release
  * Bump versioned Build-Depends on nqp

 -- Alessandro Ghedini <ghedo@debian.org>  Thu, 20 Feb 2014 14:29:40 +0100

rakudo (2013.12-1) unstable; urgency=medium

  * New upstream release
  * Bump versioned Build-Depends on nqp to 2013.12.1
  * Bump Standards-Version to 3.9.5 (no changes needed)

 -- Alessandro Ghedini <ghedo@debian.org>  Mon, 23 Dec 2013 12:27:17 +0100

rakudo (2013.11-1) unstable; urgency=low

  * New upstream release
  * Use --prefix instead of --with-nqp in Configure.pl
  * Bump versioned nqp (Build-)Depends

 -- Alessandro Ghedini <ghedo@debian.org>  Sun, 24 Nov 2013 17:46:54 +0100

rakudo (2013.10-1) unstable; urgency=low

  * New upstream release
  * Bump versioned nqp and parrot (Build-)Depends
  * Fix vcs-field-not-canonical
  * Do not Depends on parrot-devel anymore

 -- Alessandro Ghedini <ghedo@debian.org>  Fri, 01 Nov 2013 16:27:49 +0100

rakudo (2013.03-1) unstable; urgency=low

  * New upstream release
  * Bump versioned (Build-)Depends on parrot and nqp

 -- Alessandro Ghedini <ghedo@debian.org>  Thu, 04 Apr 2013 17:53:29 +0200

rakudo (2012.10-1) unstable; urgency=low

  * New upstream release
  * Bump parrot and nqp required versions
  * Bump Standards-Version to 3.9.4 (no changes needed)

 -- Alessandro Ghedini <ghedo@debian.org>  Sun, 28 Oct 2012 13:22:43 +0100

rakudo (0.1~2012.04.1-2) unstable; urgency=low

  * Dynamically generate versioned depends on nqp during build
    to fix 'Missing or wrong version of dependency ...' errors
  * Remove not needed source/options file

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 12 Jun 2012 15:20:12 +0200

rakudo (0.1~2012.04.1-1) unstable; urgency=low

  * New upstream release
  * Bump required nqp and parrot version
  * Explicitly set nqp path during configure
  * Re-word long description
  * Use dh_parrot debhelper plugin

 -- Alessandro Ghedini <ghedo@debian.org>  Fri, 08 Jun 2012 15:50:16 +0200

rakudo (0.1~2012.01-1) unstable; urgency=low

  * New upstream release
  * Depends on versioned parrot too (Closes: #653240)
  * Bump debhelper compat level to 9
  * Update debian/copyright format as in Debian Policy 3.9.3
  * Bump Standards-Version to 3.9.3
  * Update upstream copyright years
  * (Build) depend on nqp too, update parrot version
  * Build depend on libicu-dev (Closes: #669499)
  * Email change: Alessandro Ghedini -> ghedo@debian.org
  * Manually depend on parrotapi-X.Y.Z

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 24 Apr 2012 17:04:08 +0200

rakudo (0.1~2011.07-1) unstable; urgency=low

  [ Alessandro Ghedini ]
  * New upstream release
  * Bump debhelper compat level to 8
  * Update Dominique's email address
  * Bump (Build-)Depends on parrot to 3.6.0
  * Add Provides: perl6 field

  [ Dominique Dumont ]
  * debian/copyright: reformatted with config-edit
  + debian/source/option: Build process creates a config.status
    file. This option ignores it when checking source changes
  * debian/control:
    + added (Build-)Depends on libparrot3.6.0
    * Slightly changed synopsis
    * reformatted with config-edit

 -- Dominique Dumont <dod@debian.org>  Sun, 07 Aug 2011 09:51:59 +0200

rakudo (0.1~2011.04-1) unstable; urgency=low

  * New upstream release (Closes: #601862, #585762, #577502)
  * New maintainer
  * Switch to 3.0 (quilt) format
  * Update dependencies (Closes: #584498)
  * Update debian/copyright to lastest DEP5 revision
  * Do not generate/install perl6 manpage (now done by the build system)
  * Enable tests
  * Bump Standards-Version to 3.9.2 (no changes needed)
  * Do not install extra LICENSE files and duplicated docs
  * Remove debian/clean (no more needed)
  * Add Vcs-* fields in debian/control
  * Rewrite (short) description
  * Update upstream copyright years
  * Upload to unstable

 -- Alessandro Ghedini <al3xbio@gmail.com>  Tue, 17 May 2011 11:31:09 +0200

rakudo (0.1~2010.01-1) experimental; urgency=low

  * generate a manpage from docs/running.pod
  * update the d/watch file
  * New Upstream Version
    - update dependencies

 -- Ryan Niebur <ryan@debian.org>  Mon, 29 Mar 2010 22:47:34 -0700

rakudo (0.1~2009.09-3) experimental; urgency=low

  * fix build-dependencies this time (Closes: 551115)
  * remove perl6_group.* in clean

 -- Ryan Niebur <ryan@debian.org>  Fri, 16 Oct 2009 22:34:27 -0700

rakudo (0.1~2009.09-2) experimental; urgency=low

  * make sure that version 1.6.0 of parrot-devel, parrot, and libparrot-
    dev are installed

 -- Ryan Niebur <ryan@debian.org>  Mon, 05 Oct 2009 17:00:01 -0700

rakudo (0.1~2009.09-1) experimental; urgency=low

  * initial release (closes: 544399)

 -- Ryan Niebur <ryan@debian.org>  Sun, 04 Oct 2009 14:31:57 -0700
